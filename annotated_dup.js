var annotated_dup =
[
    [ "Controller", "namespace_controller.html", [
      [ "Controller", "class_controller_1_1_controller.html", "class_controller_1_1_controller" ]
    ] ],
    [ "Encoder", "namespace_encoder.html", [
      [ "Encoder", "class_encoder_1_1_encoder.html", "class_encoder_1_1_encoder" ]
    ] ],
    [ "mcp9808", "namespacemcp9808.html", [
      [ "mcp9808", "classmcp9808_1_1mcp9808.html", "classmcp9808_1_1mcp9808" ]
    ] ],
    [ "MotorDriver", "namespace_motor_driver.html", [
      [ "MotorDriver", "class_motor_driver_1_1_motor_driver.html", "class_motor_driver_1_1_motor_driver" ]
    ] ],
    [ "ResistiveTouch", "namespace_resistive_touch.html", [
      [ "ResistiveTouch", "class_resistive_touch_1_1_resistive_touch.html", "class_resistive_touch_1_1_resistive_touch" ]
    ] ]
];