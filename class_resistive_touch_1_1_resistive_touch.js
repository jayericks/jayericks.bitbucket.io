var class_resistive_touch_1_1_resistive_touch =
[
    [ "__init__", "class_resistive_touch_1_1_resistive_touch.html#a39466d7378394eeb59fd64a239266096", null ],
    [ "xRead", "class_resistive_touch_1_1_resistive_touch.html#a23e0a5ce2789bc5b400b5ea5c5172ef2", null ],
    [ "xyzRead", "class_resistive_touch_1_1_resistive_touch.html#a93b026fc64e5bf25025df661513e842c", null ],
    [ "yRead", "class_resistive_touch_1_1_resistive_touch.html#a29e9d5d1d72ab1330d5f2f906dcca0fd", null ],
    [ "zRead", "class_resistive_touch_1_1_resistive_touch.html#ac469113cf3c770acb5793eaad37e3970", null ],
    [ "X", "class_resistive_touch_1_1_resistive_touch.html#aaf95a8bad09790add1bd3429e3d10496", null ],
    [ "x_adc", "class_resistive_touch_1_1_resistive_touch.html#aed287e4b0e315df750851b03f87e5fdf", null ],
    [ "x_hi", "class_resistive_touch_1_1_resistive_touch.html#abb1a28296aa7183e715f88eea11c2ae1", null ],
    [ "x_lo", "class_resistive_touch_1_1_resistive_touch.html#ac4bd4632a5a9f413e639a2a2d1e6d382", null ],
    [ "Y", "class_resistive_touch_1_1_resistive_touch.html#afb4b2103fa772bd681cff2dae73284de", null ],
    [ "y_adc", "class_resistive_touch_1_1_resistive_touch.html#a05f399fa1b8b36e44c62273ddf3bcc9f", null ],
    [ "y_hi", "class_resistive_touch_1_1_resistive_touch.html#addcdc6ab4d52e672630edd0414914fd3", null ],
    [ "y_lo", "class_resistive_touch_1_1_resistive_touch.html#a3d63fe5fcb0f348d870f105d7c6b3eaf", null ]
];