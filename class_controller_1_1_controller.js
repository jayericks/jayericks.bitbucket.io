var class_controller_1_1_controller =
[
    [ "__init__", "class_controller_1_1_controller.html#ae584317363efa29ace5c4294ff9675a2", null ],
    [ "compute", "class_controller_1_1_controller.html#a99a630f023cc59a1a305043cf58dfde6", null ],
    [ "K1", "class_controller_1_1_controller.html#a40bba6ff6634ed5a047b017a8e224030", null ],
    [ "K2", "class_controller_1_1_controller.html#a9d7fab9e44f39aaeef27b20f90127b6e", null ],
    [ "K3", "class_controller_1_1_controller.html#ae8345bc2dd920ae0b2e1c8d531d5b91d", null ],
    [ "K4", "class_controller_1_1_controller.html#a730527f891116ea7bb116caec0c0e384", null ],
    [ "pos", "class_controller_1_1_controller.html#a13090e584fc4c0ccb3df4075f284bb60", null ],
    [ "posDot", "class_controller_1_1_controller.html#aa2aa368537eb8dee2d15e8c258154de3", null ],
    [ "theta", "class_controller_1_1_controller.html#ac0310fdda4531f5400a6787b1ddb6d32", null ],
    [ "thetaDot", "class_controller_1_1_controller.html#a2657b86b62991244445628d76f25773a", null ]
];