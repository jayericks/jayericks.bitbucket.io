var searchData=
[
  ['x_92',['X',['../class_resistive_touch_1_1_resistive_touch.html#aaf95a8bad09790add1bd3429e3d10496',1,'ResistiveTouch.ResistiveTouch.X()'],['../namespacemain7.html#a8ba2cbf3454da7650ed7b416372353f5',1,'main7.X()']]],
  ['x_5fadc_93',['x_adc',['../class_resistive_touch_1_1_resistive_touch.html#aed287e4b0e315df750851b03f87e5fdf',1,'ResistiveTouch::ResistiveTouch']]],
  ['x_5fhi_94',['x_hi',['../class_resistive_touch_1_1_resistive_touch.html#abb1a28296aa7183e715f88eea11c2ae1',1,'ResistiveTouch.ResistiveTouch.x_hi()'],['../namespacemain7.html#af063d1b6773795337c87fd725d5821c0',1,'main7.x_hi()']]],
  ['x_5flo_95',['x_lo',['../class_resistive_touch_1_1_resistive_touch.html#ac4bd4632a5a9f413e639a2a2d1e6d382',1,'ResistiveTouch.ResistiveTouch.x_lo()'],['../namespacemain7.html#a2de8c9bb0935953e38bf338f31ddf867',1,'main7.x_lo()']]],
  ['xread_96',['xRead',['../class_resistive_touch_1_1_resistive_touch.html#a23e0a5ce2789bc5b400b5ea5c5172ef2',1,'ResistiveTouch::ResistiveTouch']]],
  ['xyzread_97',['xyzRead',['../class_resistive_touch_1_1_resistive_touch.html#a93b026fc64e5bf25025df661513e842c',1,'ResistiveTouch::ResistiveTouch']]]
];
