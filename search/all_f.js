var searchData=
[
  ['sendchar_72',['sendChar',['../namespaceui.html#a2e171906a9a297098a64c16abea45909',1,'ui']]],
  ['ser_73',['ser',['../namespaceui.html#a88b09884824cde8a2a647e4a1916b2e1',1,'ui']]],
  ['set_5fduty_74',['set_duty',['../class_motor_driver_1_1_motor_driver.html#a4bb86eafa05d8e874896aef624ad14cd',1,'MotorDriver::MotorDriver']]],
  ['setposition_75',['setPosition',['../class_encoder_1_1_encoder.html#aae8ed6cb138bf8e83d7947bb137345ed',1,'Encoder::Encoder']]],
  ['ssx_76',['ssX',['../namespacemain__final.html#abcfd9aaaf44ce027255caa5582d76498',1,'main_final']]],
  ['ssy_77',['ssY',['../namespacemain__final.html#ade91bdb0128c309600a6343e67b9b765',1,'main_final']]],
  ['state_78',['state',['../namespace_lab0x01.html#af49dfbadfaec711466de0f806232bf66',1,'Lab0x01.state()'],['../namespacelab3main.html#a823cc7f0aad25e004bc061b33cba500a',1,'lab3main.state()'],['../namespaceui.html#a3b66a509386f3e2ab868be9bafc7176a',1,'ui.state()']]]
];
