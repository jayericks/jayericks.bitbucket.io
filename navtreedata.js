/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Erickson_Project", "index.html", [
    [ "Introduction", "index.html#sec_port", null ],
    [ "Weekly Lab Activities", "_weekly_labs_page.html", [
      [ "Lab 1: Finite State Machine", "_weekly_labs_page.html#sec_lab1", null ],
      [ "Lab 2: Think Fast!", "_weekly_labs_page.html#sec_lab2", null ],
      [ "Lab 3: Pushing the Right Buttons", "_weekly_labs_page.html#sec_lab3", null ],
      [ "Lab 4: Hot or Not?", "_weekly_labs_page.html#sec_lab4", null ],
      [ "Lab 5: Feelin' Tipsy?", "_weekly_labs_page.html#sec_lab5", null ],
      [ "Lab 6: Simulation or Reality?", "_weekly_labs_page.html#sec_lab6", null ],
      [ "Lab 7: Feeling Touchy", "_weekly_labs_page.html#sec_lab7", null ],
      [ "Lab 8 : Term Project Part I", "_weekly_labs_page.html#sec_lab8", null ]
    ] ],
    [ "Final Project Description", "final_proj_desc.html", [
      [ "Introduction", "final_proj_desc.html#sec_finalIntro", null ],
      [ "Model Calculations", "final_proj_desc.html#sec_finalcalc", null ],
      [ "Model Simulation", "final_proj_desc.html#sec_finalsim", null ],
      [ "EE513 Further Work", "final_proj_desc.html#sec_EE", null ],
      [ "Touch Pad", "final_proj_desc.html#sec_finalTP", null ],
      [ "Encoder", "final_proj_desc.html#sec_finalEnc", null ],
      [ "Motor", "final_proj_desc.html#sec_finalMot", null ],
      [ "Controller", "final_proj_desc.html#sec_finalCont", null ],
      [ "Final Project", "final_proj_desc.html#sec_final", null ],
      [ "Performance Video", "final_proj_desc.html#sec_finalVid", null ]
    ] ],
    [ "Final Project Calculations", "_proj_calcs.html", [
      [ "Model Calculations", "_proj_calcs.html#sec_calcsModel", null ],
      [ "System Linearization", "_proj_calcs.html#sec_calcsLin", null ]
    ] ],
    [ "Final Project Simulations", "_lab6__project.html", [
      [ "Open Loop Simulations", "_lab6__project.html#sim_OL", null ],
      [ "Closed Loop Simulations", "_lab6__project.html#sec_simCL", null ]
    ] ],
    [ "EE513 Controls", "_e_e_proj.html", [
      [ "Controls Analysis from EE513", "_e_e_proj.html#sec_head", null ]
    ] ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_e_e_proj.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';